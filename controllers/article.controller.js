const db = require('../database');
const Article = db.articles;
const articleController = {};

exports.findAll = (req, res) => {
    Article.findAll({}).then(article => {
        res.json(article);
    }).catch(err => {
        console.log(err);
        res.status(500).json({ msg: "error", details: err });
    });
};

exports.create = (req, res) => {
    Article.create(req.body).then(article => {
        res.json(article);
    }).catch(err => {
        console.log(err);
        res.status(500).json({ msg: "error", details: err });
    });
};

exports.update = (req, res) => {
    const id = req.body.id;
    Article.update(req.body,
        { where: { id: id } }).then(() => {
            res.status(200).json({ mgs: "Updated Successfully -> Customer Id = " + id });
        }).catch(err => {
            console.log(err);
            res.status(500).json({ msg: "error", details: err });
        });
};

exports.findById = (req, res) => {
    Article.findById(req.params.id).then(article => {
        res.json(article);
    }).catch(err => {
        console.log(err);
        res.status(500).json({ msg: "error", details: err });
    });
};

exports.delete = (req, res) => {
    const id = req.params.id;
    Article.destroy({
        where: { id: id }
    }).then(() => {
        res.status(200).json({ msg: 'Deleted Successfully -> Customer Id = ' + id });
    }).catch(err => {
        console.log(err);
        res.status(500).json({ msg: "error", details: err });
    });
};