const db = require('../database');
const User = db.user;

exports.findAll = (req, res) => {
    User.findAll({}).then(users => {
        res.json(users);
    }).catch(err => {
        console.log(err);
        res.status(500).json({ msg: "error", details: err });
    });
};

exports.create = (req, res) => {
    User.create(req.body)
        .then((user) => {
            res.json(user);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({ msg: "error", details: err });
        });
}

exports.update = (req, res) => {
    const id = req.body.id;
    User.update(req.body, {
        where: { id: id }
    })
        .then(() => {
            res.status(200).json({ msg: 'Updated successfully -> customer id = ' + id });
        })
        .catch((err) => {
            res.status(500).json({ msg: 'error', details: err });
        });
}

exports.findById = (req, res) => {
    user.findById(req.params.id)
        .then((user) => {
            res.json(user);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({ msg: "error", details: err });
        });
}

exports.delete = (req, res) => {
    const id = req.params.id;
    user.destroy({ where: { id: id } })
        .then(() => {
            res.status(200).json({ msg: 'Deleted successfully -> customer id = ' = id });
        })
        .catch((err) => {
            res.status(300).json({ msg: 'error', details: err });
        });
}