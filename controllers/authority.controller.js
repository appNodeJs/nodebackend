const db = require('../database');
const Authority = db.authority;

exports.findAll = (req, res) => {
    Authority.findAll({}).then(authorities => {
        res.json(authorities);
    }).catch(err => {
        console.log(err);
        res.status(500).json({ msg: "error", details: err });
    });
};

exports.create = (req, res) => {
    Authority.create(req.body)
        .then((authority) => {
            res.json(authority);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({ msg: "error", details: err });
        });
}

exports.update = (req, res) => {
    const id = req.body.id;
    Authority.update(req.body, {
        where: { id: id }
    })
        .then(() => {
            res.status(200).json({ msg: 'Updated successfully -> customer id = ' + id });
        })
        .catch((err) => {
            res.status(500).json({ msg: 'error', details: err });
        });
}

exports.findById = (req, res) => {
    Authority.findById(req.params.id)
        .then((authority) => {
            res.json(authority);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({ msg: "error", details: err });
        });
}

exports.delete = (req, res) => {
    const id = req.params.id;
    Authority.destroy({ where: { id: id } })
        .then(() => {
            res.status(200).json({ msg: 'Deleted successfully -> customer id = ' + id });
        })
        .catch((err) => {
            res.status(300).json({ msg: 'error', details: err });
        });
}