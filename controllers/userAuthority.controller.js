const db = require('../database');
const UserAuthority = db.userAuthority;

exports.findAll = (req, res) => {
    UserAuthority.findAll({}).then(userAuthorities => {
        res.json(userAuthorities);
    }).catch(err => {
        console.log(err);
        res.status(500).json({ msg: "error", details: err });
    });
};

exports.update = (req, res) => {
    const id = req.body.id;
    UserAuthority.update(req.body, {
        where: { id: id }
    })
        .then(() => {
            res.status(200).json({ msg: 'Updated successfully -> customer id = ' + id });
        })
        .catch((err) => {
            res.status(500).json({ msg: 'error', details: err });
        });
}

exports.findById = (req, res) => {
    UserAuthority.findById(req.params.id)
        .then((userAuthorities) => {
            res.json(userAuthorities);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({ msg: "error", details: err });
        });
}

exports.delete = (req, res) => {
    const id = req.params.id;
    UserAuthority.destroy({ where: { id: id } })
        .then(() => {
            res.status(200).json({ msg: 'Deleted successfully -> customer id = ' = id });
        })
        .catch((err) => {
            res.status(300).json({ msg: 'error', details: err });
        });
}