const express = require('express');
const router = express.Router();

const authorityController = require('../controllers/authority.controller');

router.get('/api/authorities', authorityController.findAll);

module.exports = router;