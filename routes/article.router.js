const express = require('express');
const router = express.Router();

const articleController = require('../controllers/article.controller');

router.get('/api/articles', articleController.findAll);
router.post('/api/articles', articleController.create);
router.get('/api/articles/:id', articleController.findById);
router.put('/api/articles', articleController.update);
router.delete('/api/articles/:id', articleController.delete);

module.exports = router;