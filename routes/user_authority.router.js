const express = require('express');
const router = express.Router();

const userAuthorityController = require('../controllers/userAuthority.controller');

router.get('/api/user-authorities', userAuthorityController.findAll);

module.exports = router;