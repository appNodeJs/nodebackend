const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();

const db = require('./database');

// delete data in data Base
// db.sequelize.sync({ force: true }).then(() => {
//     console.log('Drop and Resync with { force: true }');
//     initial();
// });

// settings
app.set('port', process.env.PORT || '3000');

// middleWares
app.use(morgan('dev')); // mesages in console
app.use(express.json()); // parsera json desde la web
// app.use(cors({ origin: 'http://localhost:4200' })); // integration with angular 
app.use(cors({origin: 'http://localhost:4200',credentials: true}));

// Routes
app.use(require('./routes/article.routes'));
app.use(require('./routes/authority.routes'));

//  Starting the server
app.listen(app.get('port'), () => {
    console.log('server on port', app.get('port'));
});

// create authorities by defect

function initial() {
    let authorities = [
        {
            name: "ROLE_ADMIN",
            description: "Not description"
        },
        {
            name: "ROLE_STAFF",
            description: "Not description"
        },
        {
            name: "ROLE_OWNER",
            description: "Not description"
        }
    ];
    const authotiry = db.authority;
    for (let i = 0; i < authorities.length; i++) {
        authotiry.create(authorities[i]);
    }
}
