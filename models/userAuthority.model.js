const user = require('../models/user.model');
const authority = require('../models/authority.model');

module.exports = (sequelize, Sequelize) => {
    const userAuthority = sequelize.define('user_authority', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING(255)
        }
    });
    return userAuthority;
}