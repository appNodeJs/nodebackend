module.exports = (sequelize, Sequelize) => {
    const Article = sequelize.define('article', {
        name: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        }
    });
    return Article;
}