module.exports = (sequelize, Sequelize) => {
    const user = sequelize.define('user', {
        name: {
            type: Sequelize.STRING(255)
        },
        email: {
            type: Sequelize.STRING(255),
            unique: true,
            validate: {
                isEmail: true
            }
        }
    });
    return user;
}