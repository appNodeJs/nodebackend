module.exports = (sequelize, Sequelize) => {
    const authority = sequelize.define('authority', {
        name: {
            type: Sequelize.STRING(255)
        },
        description: {
            type: Sequelize.TEXT
        }
    });
    return authority;
}